#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_onc.mk

COMMON_LUNCH_CHOICES := \
    lineage_onc-user \
    lineage_onc-userdebug \
    lineage_onc-eng
